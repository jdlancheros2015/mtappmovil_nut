/* praxis colombia 
 * MT_Appmovil v1.2.2
 * validar conexion de red del dispositivo */
var respuesta = "";
var urlWS = "http://52.0.134.0:8080/MT_AppMovil/";
checkConnection = function () {
    var networkState = navigator.connection.type;
    var states = {};
    states[Connection.UNKNOWN] = 'Unknown';
    states[Connection.ETHERNET] = 'Eth';
    states[Connection.WIFI] = 'WiFi';
    states[Connection.CELL_2G] = '2G';
    states[Connection.CELL_3G] = '3G';
    states[Connection.CELL_4G] = '4G';
    states[Connection.CELL] = 'GC';
    states[Connection.NONE] = 'NO';
    return states[networkState];
}
/* autenticacion contra el servidor */
loginOnile = function (gnd, $scope) {
    if (window.XMLHttpRequest) {
        /* Objeto para IE7+, Firefox, Chrome, Opera, Safari */
        xmlhttp = new XMLHttpRequest();
    }
    else {
        /* Objeto para IE6, IE5 */
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.open("POST", urlWS + "/HTTPWrapperWS?wservice=1&param1=" + $scope.user + "&param2=" + $scope.pwd + "", false);
    xmlhttp.send();
    /* Obtenemos un objeto XMLDocument con el contenido del archivo xml del servidor */
    xmlText = xmlhttp.responseText;
    //usuario = x2js.xml_str2json(uncompress(window.atob(xmlText)));
    // usuario = x2js.xml_str2json(uncompressT(xmlText));
    if (xmlText) {
        uncompressT(xmlText, function (datosdes) {
            usuario = x2js.xml_str2json(datosdes);
        })
    }
    if (usuario) {
        cargando();
        validarUsuario(gnd, $scope);
        guardarArchivo("entrada/login.xml", xmlText);
        gnd = true;
    } else {
        
    }
}
/* autenticacion localmente del dispositivo */
loginOffline = function (gnd, $scope) {
    if (confirm('Recuerde que ya debió realizar el proceso de sincronización. ¿Desea Continuar?')) {
        validarUsuario(gnd, $scope);
    } else {
        apagacargando();
        return;
    }
}
/* desloguearse de la aplicacion */
function salir() {
    if (cantarchivsal == true) {
        try {
            navigator.notification.confirm(
                    '¿Quiere sincronizar su gestión con el servidor ?', //titulo de la ventana
                    onConfirmenv, // funciÃ³n callback que recibe el parametro de respuesta
                    'Sincronizando MT_AppMovil', //mensaje de la ventana
                    'No,Si'         // label de los botones
                    );
        } catch (e) {
            alert('Exception: ' + e.message);
        }
    } else {
        try {
            navigator.notification.confirm(
                    '¿Esta seguro que quiere salir de MT_AppMovil?', //titulo de la ventana
                    onConfirm, // funciÃ³n callback que recibe el parametro de respuesta
                    'Cerrar MT_AppMovil', //mensaje de la ventana
                    'No,Si'         // label de los botones
                    );
        } catch (e) {
            alert('Exception: ' + e.message);
        }
    }
}
/* confirmacion de envio de gestion local */
function onConfirmenv(res) {
    if (res == 1)//1 es no 2 es si
    {
        try {
            navigator.notification.confirm(
                    '¿esta seguro que quiere salir de MT_AppMovil?', //titulo de la ventana
                    onConfirm, // función callback que recibe el parametro de respuesta
                    'Cerrar MT_AppMovil', //mensaje de la ventana
                    'No,Si'         // label de los botones
                    );
        } catch (e) {
            alert('Exception: ' + e.message);
        }
    }
    if (res == 2)//1 es no 2 es si
    {//cerramos la app      
        subirgestion();
        if (conexion != 'NO') {
            //se sincroniza 
        }
        try {
            navigator.notification.confirm(
                    '¿esta seguro que quiere salir de MT_AppMovil?', //titulo de la ventana
                    onConfirm, // funciÃ³n callback que recibe el parametro de respuesta
                    'Cerrar MT_AppMovil', //mensaje de la ventana
                    'No,Si'         // label de los botones
                    );
        } catch (e) {
            alert('Exception: ' + e.message);
        }
    }
}
/* confirmacion de salida de MT_Appmovil */
function onConfirm(res) {
    if (res == 2)//1 es no 2 es si
    {//cerramos la app      
        limpiarVariables();
        navigator.app.exitApp();
    }
}
/* se limpian las variables para liberar memoria */
function limpiarVariables() {
    usuario = null
    misProyectos = null
    miot = null
    ots = null
    miotact = null;
    autenticado = null;
    idcap = null;
    camino = null;
    gnd = null;
    detallecap = null;
    indicadores = null;
    idindsel = null;
    ots = null;
    idOt = null;
    listAlert = null;
    listAlertgestloc = null;
    photo = null;
    sendPhoto = null;
    imageData = null;
    alrgest = null;
    subido = null;
}