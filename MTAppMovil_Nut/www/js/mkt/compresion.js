/* praxis colombia 
 * MT_Appmovil v1.2.2
 * libreria de compresion y descompresion
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function uncompressT(datos1, callback) {
    var zip = new JSZip(datos1, {base64: true});
    if (zip != null) {
        var data = zip.file('compress').asText();
        callback(data);
    }
}
/**/
uncompress = function (datos1) {
    try {
        var datos = datos1.split('').map(function (e) {
            return e.charCodeAt(0);
        });
        var inflate = new Zlib.Unzip(datos);
        var filenames = inflate.getFilenames();
        var output = inflate.decompress(filenames[0]);
        var s = "";
        for (var i = 0, l = output.length; i < l; i++)
            s += String.fromCharCode(output[i]);
        output = s;
    }
    catch (err) {
        alert(err)
        console.log("Error descomprimiendo" + err);
    }
    return output;
}
/**/
compress = function (datos1) {
    try {
        var datos = datos1.split('').map(function (e) {
            return e.charCodeAt(0);
        });
        var inflate = new Zlib.Unzip(datos);
        var filenames = inflate.getFilenames();
        var output = inflate.decompress(filenames[0]);
        var s = "";
        for (var i = 0, l = output.length; i < l; i++)
            s += String.fromCharCode(output[i]);
        output = s;
    }
    catch (err) {
        console.log("Error descomprimiendo" + err);
    }
    return output;
}
/**/
compressToB64 = function (datos1) {
    try {
        var zip = new Zlib.Zip();
        // plainData1
        zip.addFile(datos1, {
            filename: stringToByteArray('gestion.xml')
        });
        var compressed = zip.compress();
        return btoa(compressed);
    }
    catch (err) {
        console.log("Error comprimiendo" + err);
    }
    return null;
}
/**/
function unzipDataURI(dataURI, callback) {
    zip.createReader(new zip.Data64URIReader(dataURI), function (zipReader) {
        zipReader.getEntries(function (entries) {
            entries[0].getData(new zip.TextWriter(), function (data) {
                zipReader.close();
                callback(data);
            });
        });
    }, onerror);

    function onerror(error) {

        console.log(error);
    }
}
function zipTextToB64zip(archivo, callback) {
    ///creamos un writer de Base 64
    zip.createWriter(new zip.BlobWriter("application/zip"), function (zipWriter) {
        // leemos el texto que le estamos pasando
        zipWriter.add("1", new zip.TextReader(archivo), function () {
            ///cerramos y llamamos al callback
            zipWriter.close(callback);
        });
    }, onerror);

    function onerror(error) {
        console.log(error);
    }
}