/* se crea el archivo */
guardarArchivo = function (momArchivo, archivo) {
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
    function gotFS(fileSystem) {
        fileSystem.root.getFile("appmovil/" + momArchivo, {create: true, exclusive: false}, gotFileEntry, fail);
    }
    function gotFileEntry(fileEntry) {
        fileEntry.createWriter(gotFileWriter, fail);
    }
    function gotFileWriter(writer) {
        writer.write(archivo);

    }
    function fail(error) {
        console.log(error.code);
    }
}
/**/
function xmlToJson2(text) {

    return JSON.parse(text);
}
/*convierte un xml en json */

/**/
/* convierte un objeto en xml */
function XMLString(object, name, recursiveCall) {
    var xml = "", i;
    var isArray = typeof (object) === 'object' && typeof (object.length) === 'number' && !(object.propertyIsEnumerable('length')) && typeof (object.splice === 'function');

    if (!recursiveCall) {
        xml += "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
        xml += "<" + name + ">";
    } else if (!isArray) {
        xml += "<" + name + ">";
    }

    if (typeof (object) === "undefined" || object == null)
        return ""; /* vacio */
    else if (typeof (object) === "object")
        for (i in object)
            xml += XMLString(object[i], isArray ? name : i, true); // Un objeto o array
    else
        xml += object; /* Un texto */

    if (!isArray || !recursiveCall)
        xml += "</" + name + ">";

    return xml;
}

precargado = function ()
{
    var archivo1 = select('#archivo').files[0],
            archivo = [archivo1];
    cargarArchivo(archivo);
}
/* */
function leerArchivo(fileName, call) {
    var callBack = call;
    var content = null;
    var args = arguments;
    var context = this;
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (
            fileSystem) {
        fileSystem.root.getFile("appmovil/" + fileName, null, function (
                fileEntry) {
            fileEntry.file(function (file) {
                var reader = new FileReader();
                reader.onloadend = function (evt) {
                    args = [].splice.call(args, 0);
                    args[0] = evt.target.result;
                    args.splice(1, 1);
                    callBack.apply(context, args);
                }
                reader.readAsText(file);
            });
        }, fail);
    }, fail);
    function fail(error) {
        resultado = error;
    }
}
/* */
function borrarArchivo(fileName, call) {
    var callBack = call;
    var content = null;
    var args = arguments;
    var context = this;
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (
            fileSystem) {
        fileSystem.root.getFile("appmovil/" + fileName, null, function (
                fileEntry) {
            fileEntry.remove();
            callBack;
        }, fail);
    }, fail);
    function fail(error) {
        resultado = error;
        console.log(error.code);
    }

}
/* */
function listarDirectorio(dir, call) {
    var callBack = call;
    var content = null;
    var args = arguments;
    var context = this;
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        fileSystem.root.getDirectory("appmovil/" + dir, {
            create: true
        }, function (directory) {
            var directoryReader = directory.createReader();
            directoryReader.readEntries(function (entries) {
                var i;
                for (i = 0; i < entries.length; i++) {
                    cantarchsal = entries.length;
                    args = [].splice.call(args, 0);
                    args[0] = dir + "/" + entries[i].name;
                    args.splice(1, 1);
                    callBack.apply(context, args);
                }
            }, function (error) {
                console.log(error.code);
            });

        });
    }, function (error) {
        console.log(error.code);
    });
}
/* funcion de camara captura fotos */
tomarfoto = function () {
    document.getElementById('photo').style.visibility = "visible";
    navigator.camera.getPicture(onPhotoDataSuccess, onFail,
            {quality: 80, allowEdit: false, encodingType: Camera.EncodingType.JPEG, destinationType: navigator.camera.DestinationType.DATA_URL});
}
onPhotoDataSuccess = function (imageData) {
    var photo = document.getElementById('photo');
    sendPhoto = document.getElementById('sendPhoto');
    fechacap = Date();
    imagefoto = "data:image/jpeg;base64," + imageData;
    navigator.geolocation.getCurrentPosition(onSuccess, onFail);
    
    if (photo != null) {
        photo.src = imagefoto;
        photo.style.visibility = "visible";
        document.getElementById('capfoto').style.display = 'none';
    }
}
/* captura geoposicionamiento de la captura */
onSuccess = function (position) {
    gpscap = position.coords.latitude + " || " + position.coords.longitude;
}
/* si falla en alguna accion */
onFail = function (message) {
    alert('Falló porque: ' + message);
}
/* /////////////////// */