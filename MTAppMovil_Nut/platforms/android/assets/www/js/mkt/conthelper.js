/* praxis colombia 
 * MT_Appmovil v1.2.2
 * controlador general
 */
var vistas = [];
var vistaAct = -1;
var vistaAnt = -1;
var ruta = "";
var temp = "";

/* array de vistas de MT_Appmovil */
vistas[0] = "detalleCapitulo.html";
vistas[1] = "capituloNivel.html";
vistas[2] = "detalleInd.html";
vistas[3] = "indicadorNivel.html";
vistas[4] = "detalleIndGestion.html";
vistas[5] = "MisAlertas.html";
vistas[6] = "detalleCapsubcat.html";

/* inicializar las variables de las vistas */
function initVistaDetalle($scope, vistaact) {
    if (miotant != undefined && miotant.nomNivel != undefined && miotant.nomNivel != miotact.nomNivel) {

        if (ruta.indexOf(miotact.nomNivel) == -1 && miotact.nomNivel!=undefined) {
            ruta = ruta + "->" + miotact.nomNivel;
        }
    }
    if (vistaAct == 5) {
        ruta = "Mis Alertas";
    }
    if (miot.MiOTDto.idUsr == miotact.nomNivel) {
        ruta = miotact.nomNivel;
    }

    $scope.color = "";
    isEnSalida($scope);
    $scope.nivelact = miotact;
     
    $scope.selectedRow = null;
    $scope.buscado = null;
    $scope.indact = idindsel;
    $scope.idcapact = idcap;
    $scope.camino = ruta;
    vistaAct = vistaact;
}
/* bajar de nivel en la navegacion del usuario */
function bajarNivelCapitulo($scope) {
    if ($scope.selectedRow == null) {
        alert('Por favor seleccione el registro');
        apagacargando();
    }
    else {
        if (miotact.nivel <= 1 && (vistaAct == 1 || vistaAct == 3)) {
            verDetalleCapitulo($scope);
            apagacargando();
            return;
        } else if (miotact.nivel < 1) {
            if (miotact.capitulos[idcap].id > 1) {
                miothist=miotant;
                miotant=miotact;
                miotact=miotact.capitulos[idcap];
                categorias = miotact.ins;
                $scope.ons.navigator.getCurrentPage().destroy();
                $scope.ons.navigator.pushPage(vistas[6], {animation: "fade"});
                setTimeout(apagacargando, 1000);
            }
            apagacargando();
            return;
        }
        else {
            
            vistaAnt = vistaAct;
            miotant = miotact;
            if (vistaAct == 0) {
                $scope.ons.navigator.getCurrentPage().destroy();
                $scope.ons.navigator.pushPage(vistas[1], {animation: "fade"});
                setTimeout(apagacargando, 1000);
            } else if (vistaAct == 2) {
                $scope.ons.navigator.getCurrentPage().destroy();
                $scope.ons.navigator.pushPage(vistas[3], {animation: "fade"});
                setTimeout(apagacargando, 1000);
            } else if (vistaAct == 1 || vistaAct == 3) {
                if (vistaAct == vistaAnt) {
                    if (miotact.subNiveles[$scope.selectedRow]) {
                        miotact = miotact.subNiveles[$scope.selectedRow];
                        miothist=miotact;
                    } else if ($scope.selectedRow != null) {
                        miotact = miotact.subNiveles;
                    }
                    $scope.ons.navigator.getCurrentPage().destroy();
                    $scope.ons.navigator.pushPage(vistas[vistaAct], {animation: "fade"});
                    setTimeout(apagacargando, 1000);
                }
            }
        }
    }
}
/* retroceder un nivel en la navegacion del usuario */
function nivelSup($scope) {
    if (vistaAnt != -1) {
        if (miotant != undefined) {
            miotact = miotant;
            apagacargando();
        }
        if (ruta.lastIndexOf('->') > 0) {
            ruta = ruta.substr(0, ruta.lastIndexOf('->'));
        }
        if(vistaAct==6){
            miotact=miotant.capitulos[idcap];
            visttemp=6;
        }
        else if(vistaAct==4 && vistaAnt!=5){
            miotact=miotant.capitulos[idcap];
            visttemp=6;
        }else{
            visttemp = vistaAnt; 
        }
        vistaAnt = vistaAct;
        $scope.ons.navigator.getCurrentPage().destroy();
        $scope.ons.navigator.pushPage(vistas[visttemp], {animation: "fade"});
        setTimeout(apagacargando, 1000);
    } else {
        apagacargando();
    }
}
/* ver el detalle del capitulo en la navegacion del usuario */
function verDetalleCapitulo($scope) {
    if ($scope.selectedRow == null) {
        alert('Seleccione el registro');
        apagacargando();
    }
    else {
        if (vistaAct === 0) {
            miotant = miotact;
            vistaAnt = vistaAct;
            $scope.ons.navigator.getCurrentPage().destroy();
            $scope.ons.navigator.pushPage(vistas[2], {animation: "fade"});
            setTimeout(apagacargando, 1000);
        }
        else if (vistaAct === 1) {
            var res = verNivel($scope);
            if (res) {
                vistaAnt = vistaAct;
            }
        } else if (vistaAct === 3) {
            var res = verIndicadorSel($scope);
            if (res) {
                vistaAnt = vistaAct;
                $scope.ons.navigator.getCurrentPage().destroy();
                $scope.ons.navigator.pushPage(vistas[2], {animation: "fade"});
                setTimeout(apagacargando, 1000);
            }
        } else if (vistaAct === 4) {
            miotant = miotact;
            vistaAnt = vistaAct;
            $scope.ons.navigator.getCurrentPage().destroy();
            $scope.ons.navigator.pushPage(vistas[2], {animation: "fade"});
            setTimeout(apagacargando, 1000)
        } else {
            alert('Operación no permitida');
            apagacargando();
        }
    }
}
/* regresar a mis capitulos en la  navegacion del usuario */
function irMiCalificacion($scope) {
    miotact = miot.MiOTDto.nivel;
    ruta = miotact.nomNivel;
    vistaAnt = vistaAct;
    idindsel = idcap = 0;
    gestion = false;
    $scope.indact = idindsel;
    $scope.idcapact = idcap;
    initVistaDetalle($scope, 0);
    $scope.ons.navigator.getCurrentPage().destroy();
    $scope.ons.navigator.pushPage(vistas[1], {animation: "fade"});
    setTimeout(apagacargando, 1000);

}
/* ver los indicadores de capitulo en la negacion del usuario */
function verIndicadorSel($scope) {
    if ($scope.selectedRow == undefined) {
        alert('Indique el nivel');
        apagacargando();
        return false;
    }
    else {
        if (miotact.nivel > 0) {
            if (miotact.subNiveles[$scope.selectedRow]) {
                if (!Array.isArray(miotact.subNiveles[$scope.selectedRow].capitulos[idcap].ins)) {
                    alert("El capítulo no contiene indicadores");
                    apagacargando();
                    return false;
                } else {
                    miotant = miotact;
                    miotact = miotact.subNiveles[$scope.selectedRow];
                    $scope.nivelact = miotact;
                    return true;
                }
            } else {
                if (!Array.isArray(miotact.subNiveles.capitulos[idcap].ins)) {
                    alert("El capítulo no contiene indicadores");
                    apagacargando();
                    return false;
                } else {
                    miotant = miotact;
                    miotact = miotact.subNiveles;
                    return true;
                }
            }
        }
    }
}
/* ver el nivel de un capitulo en la navecio del usuario */
function verNivel($scope) {
    isEnSalida($scope);
    if ($scope.selectedRow == undefined && vistaAct != 2) {
        alert('Indique el nivel');
        apagacargando();
        return false;
    } else if (vistaAct == 2) {
        $scope.ons.navigator.pushPage(vistas[0], {animation: "fade"});
        setTimeout(apagacargando, 1000);
    }
    else {
        if (miotact.nivel > 0) {
            if (miotact.subNiveles[$scope.selectedRow]) {
                miotant = miotact;
                miotact = miotact.subNiveles[$scope.selectedRow];
                vistaAnt = vistaAct;
                $scope.ons.navigator.getCurrentPage().destroy();
                $scope.ons.navigator.pushPage(vistas[0], {animation: "fade"});
                setTimeout(apagacargando, 1000);
            } else if ($scope.selectedRow != null) {
                miotant = miotact;
                miotact = miotact.subNiveles;
                vistaAnt = vistaAct;
                $scope.ons.navigator.getCurrentPage().destroy();
                $scope.ons.navigator.pushPage(vistas[0], {animation: "fade"});
                setTimeout(apagacargando, 1000);
            }
        }
        return false;
    }
}
/* despliega la ventana de busqueda de nivel en la app */
function dialogBusqueda($scope) {
    navigator.notification.prompt(
            'Por favor indique el Nivel a buscar', // message
            function (results) {
                nivelbus = results.input1;
                var ob = "";
                if (nivelbus && results.buttonIndex == '1')
                {
                    $scope.buscado = null;
                    $scope.camino = "";
                    buscarnivel(miot.MiOTDto.nivel, nivelbus, ob, $scope);
                    if ($scope.buscado != null) {
                        miotant = miotact;
                        vistaAnt = vistaAct;
                        $scope.nivelact = $scope.buscado;
                        miotact = $scope.nivelact;
                        $scope.ons.navigator.pushPage(vistas[0], {animation: "fade"});
                        apagacargando();
                    } else {
                        alert('No se encontro el nivel:' + nivelbus);
                        apagacargando();
                    }
                } else {
                    apagacargando();
                }
            },
            'Busqueda', // titulo ventana
            ['Buscar', 'Cerrar'], // botones
            ''                 // texto preestablecido
            );
}
/**/
function isMtabla($scope, dato) {
    if (Array.isArray(dato)) {
        return true;
    } else {
        return false;
    }
}
/* lleva a la vista de gestion indicador */
function irGestionarInd($scope) {
    
    vistaAnt = vistaAct
    $scope.ons.navigator.getCurrentPage().destroy();
    $scope.ons.navigator.pushPage(vistas[4], {animation: "fade"});
    setTimeout(apagacargando, 1000);
}
/* me lleva a la vista de las alertas configuradas para ese proyecto */
function irMisAlertas($scope) {
    miotant = miotact
    vistaAnt = vistaAct
    $scope.ons.navigator.getCurrentPage().destroy();
    $scope.ons.navigator.pushPage(vistas[5], {animation: "fade"});
    setTimeout(apagacargando, 2000);
}